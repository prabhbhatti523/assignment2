package com.example.ponggame;

import android.graphics.Rect;

public class Ball {

    private Rect ball;

    private int ballXPos;
    private int ballYPos;



    public Ball( int x, int y)
    {
        this.ballXPos = x;
        this.ballYPos = y;


    }


    public Rect getBall() {
        return ball;
    }

    public void setBall(Rect ball) {
        this.ball = ball;
    }

    public int getBallXPos() {
        return ballXPos;
    }

    public void setBallXPos(int ballXPos) {
        this.ballXPos = ballXPos;
    }

    public int getBallYPos() {
        return ballYPos;
    }

    public void setBallYPos(int ballYPos) {
        this.ballYPos = ballYPos;
    }
}

