package com.example.ponggame;

import android.graphics.Rect;

public class Racket {
    private Rect racket;

    private int racketXPos;
    private int racketYPos;


    public Racket( int x, int y)
    {
        this.racketXPos = x;
        this.racketYPos = y;


    }

    public Rect getRacket() {
        return racket;
    }

    public void setRacket(Rect racket) {
        this.racket = racket;
    }

    public int getRacketXPos() {
        return racketXPos;
    }

    public void setRacketXPos(int racketXpos) {
        this.racketXPos = racketXpos;
    }

    public int getRacketYPos() {
        return racketYPos;
    }

    public void setRacketYPos(int racketYPos) {
        this.racketYPos = racketYPos;
    }
}